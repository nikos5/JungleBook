package gr.ihu.ict.msc.junglebook;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import gr.ihu.ict.msc.junglebook.model.Photo;

public class PhotoViewHolder extends RecyclerView.ViewHolder {
    TextView photoDescription;
    public PhotoViewHolder(View itemView) {
        super(itemView);
        photoDescription = itemView.findViewById(R.id.photoRecyclerId);

    }
}
