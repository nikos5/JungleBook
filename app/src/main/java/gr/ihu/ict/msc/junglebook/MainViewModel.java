package gr.ihu.ict.msc.junglebook;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Arrays;
import java.util.List;

import gr.ihu.ict.msc.junglebook.client.PhotoApiClient;
import gr.ihu.ict.msc.junglebook.model.Photo;

public class MainViewModel extends ViewModel {
    private MutableLiveData<List<Photo>> photos;
    private PhotoApiClient photoApiClient  = new PhotoApiClient();


    public LiveData<List<Photo>> getPhotos(String filterSelection) {
        if (photos == null) {
            photos = new MutableLiveData<>();
            loadPhotos(filterSelection);
        }
        return photos;
    }

    private void loadPhotos(String filterSelection) {
        photos = photoApiClient.getFilteredPhotos(filterSelection);
    }
}

